//Cache the generate button
var generateButton = document.getElementById('generate');

//Listen for the button click
generateButton.addEventListener('click', generateBeer);

//Call the beer api
function generateBeer(){
    //Call the beer API with fetch
    fetch('https://api.punkapi.com/v2/beers/random')
    //return the promise object as JSON
    .then((response) => response.json())
    //Get the actual response value
    .then((data) => {
        //pass the response array to the function that builds that output
        buildOutput(data);
        //catch the error
    }).catch((err) => {
        //handle the error
        hasError(err);
    })
}

//Alert the user that something went wrong

function hasError(err){
    alert(`I'm Sorry, Something went wrong, please try again`);
    return;
}

function buildOutput(data){
    //Clear out the previous beer fact
    document.getElementById('beerFacts').innerHTML = '';

    //cache the div used to hold the entire API response
    var responseDiv = document.getElementById('response');
    
    //unhide the respose div
    responseDiv.style.display = 'block';
    //Cache the div for the image
    var imageDiv = document.getElementById('beerImage');
    imageDiv.innerHTML = `<img src="${data[0].image_url}" width="50"/>`;

    //Cache the parent that everything will be appended to
    var cardParent = document.getElementById('beerFacts');
    
    //Create the card body div
    var cardBody = document.createElement('div');
    cardBody.setAttribute('class', 'card-body');
    
    //Create the title heading
    var beerHeadingName = document.createElement('h4');
    beerHeadingName.setAttribute('class', 'card-title');
    beerHeadingName.innerHTML = `${data[0].name} - ${data[0].tagline}`;

    //Create the card text (beer description)
    var beerDesc = document.createElement('p');
    beerDesc.setAttribute('class', 'card-text');
    beerDesc.textContent = data[0].description;

    //Create the card footer
    var cardFooter = document.createElement('div');
    cardFooter.setAttribute('class', 'card-footer');

    //create the small div
    var cardFooterText = document.createElement('small');
    cardFooterText.setAttribute('class', 'text-muted');
    cardFooterText.innerHTML = `FIRST BREWED: ${data[0].first_brewed}&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;ALC: ${data[0].abv}%&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;IBU: ${data[0].ibu}%&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;SRM: ${data[0].ebc}%&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;PH: ${data[0].ph}`;

    //Append the small element to the footer div
    cardFooter.appendChild(cardFooterText);

    //Append the heading to the card body
    cardBody.appendChild(beerHeadingName);
    //Append the description text to the card body also
    cardBody.appendChild(beerDesc);

    //Append the card body to the parent
    cardParent.appendChild(cardBody);
    //Append the footer to the card parent
    cardParent.appendChild(cardFooter);
}

/*
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Special title treatment</h4>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
    </div>
    <div class="card-footer">
        <small class="text-muted">Last updated 3 mins ago</small>
    </div>
</div>
*/