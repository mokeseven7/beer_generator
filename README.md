# README #

Beer generator created using JS Fetch API by Mike McGrath

### View A Live Demo ###

http://dopefruit.com/beer/

### API Credit ###

This app is powered by the awesome free API @ https://punkapi.com/

